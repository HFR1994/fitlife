//
//  RecipeDetailViewController.swift
//  FitLife
//
//  Created by Lawrie Cate on 20/03/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class RecipeDetailViewController: UIViewController {
    var receta:Recipe!
    @IBOutlet weak var descLabel: UILabel!
   
    @IBOutlet weak var imageVew: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = receta.title
        //self.tituloLabel.text = receta.title
        var fulltext:String = "Ingredientes:\n"

        for line in receta.ingredients! {
            fulltext += "- " + line + "\n"
         
        }
        fulltext += "\n Procidimiento:\n"
        var i = 1;
        for line in receta.steps! {
            fulltext += String(i) + ": " + line + "\n"
            i += 1
        }
        self.descLabel.text = fulltext
        
        if let image = UIImage(named:receta.image!) {
            imageVew?.image = image
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
