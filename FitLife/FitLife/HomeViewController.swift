//
//  HomeViewController.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 3/22/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit
import iCarousel

class HomeViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, iCarouselDataSource, iCarouselDelegate{

    var items: [String] = []
    var pictures: [UIImage?] = []
    @IBOutlet var carousel: iCarousel!
    
    var IndexArray = ["Desayuno","Colación","Comida","Cena"]
    var FoodArray=[
    "Vaso de Leche Semidescremada 450ml",
    "Hot cake de león",
    "3 almendras",
    "Mariposas con alas de Pescado",
    "Agua 600ml",
    "1 manzana",
    "Sopa de verduras",
    "Conejo Feliz",
    "Agua de Mango",
    "Vaso de Leche Semidescremada 450ml",
    "1 Mango",
    "Pan con Mermelada"
    ]
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return IndexArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return IndexArray[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func setNames(_ indexPath: IndexPath) -> String{
        var names=""
        
        
        switch indexPath[0]{
        case 0:
            names="Desayuno"
        case 1:
            names="Colación"
        case 2:
            names="Comida"
        case 3:
            names="Cena"
        default:
            names="Alimento"
        }
        
        switch indexPath[1]{
        case 0:
            names=names+" 0"
        case 1:
            names=names+" 1"
        case 2:
            names=names+" 2"
        default:
            names=names+""
        }
        
        
        return FoodArray[(indexPath[0]*3)+indexPath[1]]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as! TableViewCell
        cell.lblUserName.text=setNames(indexPath)
        return cell
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pictures.append(UIImage(named:"Picture1.jpg"))
        pictures.append(UIImage(named:"Picture2.jpg"))
        pictures.append(UIImage(named:"Picture3.jpg"))
        items.append("Catarinas Caprese")
        items.append("Sándwhich Con Atun")
        items.append("Nuggets de Pollo")
    }
    
    func changeSlide(){
        carousel.scroll(byNumberOfItems: 1, duration: 0.25)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        carousel.type = .linear
        _ = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(self.changeSlide), userInfo: nil, repeats: true);
    }
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return pictures.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        var image: UIImageView
        var itemView: UIImageView
        var label:UILabel
        
        //reuse view if available, otherwise create a new view
        if let view = view as? UIImageView {
            itemView = view
            //get a reference to the label in the recycled view
            image = itemView.viewWithTag(1) as! UIImageView
            label = itemView.viewWithTag(2) as! UILabel
        } else {
            //don't do anything specific to the index within
            //this `if ... else` statement because the view will be
            //recycled and used with other index values later
            itemView = UIImageView(frame: CGRect(x: 0, y: 0, width: 250, height: 250))
            itemView.image = UIImage(named: "background.jpg")
            itemView.contentMode = .center
            
            image = UIImageView(frame: itemView.bounds)
            image.backgroundColor = .clear
            image.frame.origin.y -= 20.0
            image.contentMode = .scaleAspectFit
            image.tag = 1
            itemView.addSubview(image)
            
            label = UILabel(frame:itemView.bounds)
            label.frame.origin.y += 70.0
            label.textColor = .white
            label.backgroundColor = .clear
            label.textAlignment = .center
            label.font = label.font.withSize(20)
            label.tag = 2
            itemView.addSubview(label)
        }
        
        //set item label
        //remember to always set any properties of your carousel item
        //views outside of the `if (view == nil) {...}` check otherwise
        //you'll get weird issues with carousel item content appearing
        //in the wrong place in the carousel
        
        label.text = "\(items[index])"
        image.image = imageWithImage(image: self.pictures[index]!, scaledToSize: CGSize.init(width: CGFloat.init(110), height:CGFloat.init(60)))
        return itemView
    }
    
    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: newSize.width, height: newSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.05
        }else if(option == .wrap){
            return 1
        }
        return value
    }
    
    var index = [0,1]
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index[0]=indexPath[0]
        index[1]=indexPath[1]
    }
    
    
    var HC = [
        ["½ taza de harina",
         "1 pieza de huevo",
         "¼ de taza de leche",
         "½ cucharadita de royal",
         "1 cucharadita de azúcar",
         "1 cucharadita de mantequilla",
         "2 piezas de mandarina o naranja",
         "½ pieza de plátano",
         "2 moras azules",
         "½ vaso de leche semidescremada"],
        ["Mezclar la harina , el royal y el azúcar en un bowl",
         "Agregar huevo, la leche, y una cucharadita de mantequilla derretida y mezclar hasta que se haya formado una mezcla uniforme.",
         "Calentar el satén y poner tantita mantequilla",
         "Poner un poco de la mezcla en el sartén para hacer el hotcake"]
    ]
    
    var MA = [
        ["1 filete de pescado",
         "½ taza de chicharos",
         "¼ de pieza de papa",
         "¼ de taza de zanahoria",
         "1 pieza de jitomate bola",
         "¼ de taza de harina de trigo",
         "2 cucharaditas de aceite",
         ],
        [
            "Lavar y desinfectar la verdura",
            "Colocar agua suficiente en una olla junto con las verduras, hervir aproximadamente durante 20 min.",
            "Cortar el filete de pescado den 2 piezas y caperarlo en harina.",
            "Sacar las verduras del fuego y dejar enfriar.",
            "Poner al horno los filetes hasta que queden crujientes.",
            "Cortar el jitomate en forma de flor y colocarlo en la esquina del plato",
            "Acomodar las bolitas parisiene como forma de cuerpo de la mariposa",
            "Colocar los filetes en formas de alas",
            "Colocar los chicharos y zanahorias como si fuera el pasto debajo del plato.",
            ]
    ]
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! AlimentoViewController
        destination.alimento = (index[0]==0) ? HC : MA
    }

    
}
