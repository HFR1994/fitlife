//
//  RecipeIngredientsViewController.swift
//  FitLife
//
//  Created by Lawrie Cate on 01/04/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class RecipeIngredientsViewController: UITableViewController {
     var ingredients = [Ingredient]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return datos.count
        
        //paso 5 remplazar el uso de datos por nuevoArray
        return (ingredients.count)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ingredient = ingredients[indexPath.row] as Ingredient
        let cell = tableView.dequeueReusableCell(withIdentifier: "Ingredient", for: indexPath)
        
        // Configure the cell...
        //cell.textLabel?.text = datos[indexPath.row]
        //paso 6 remplazar el uso de datos por nuevoArray
        //objeto marca es un diccionario que contiene marca y agencias
        var title = "NIL "
        if(ingredient.title != nil) {
            title = ingredient.title!
        }
        let s:String = title as String
        
        cell.textLabel?.text=s
        
        return cell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
