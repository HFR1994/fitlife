//
//  RecipesTableViewController.swift
//  FitLife
//
//  Created by Lawrie Cate on 20/03/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class RecipesTableViewController: UITableViewController {
    private let datos = [
        Recipe(id:1,title: "Catarinas caprese", image: "recipe1.png", steps:
            ["Lavar y desinfectar tomates. ",
            "Cortar el queso mozzarella en 3 rodajas.",
            "Cortar los tomates cherry a la mitad.",
            "En la rodaja de queso mozzarella poner una hoja grande de albahaca, después poner la mitad del tomate cherry arriba del albahaca. ",
            "Cortar las aceitunas negras para formar la cabeza de la Catarina, después cortar pedazos muy pequeños para hacer los puntos de las catarinas. ",
            "Poner aceite de oliva en cada rebanada de queso.  ",
            "Montaje: estructurado; piramidal (poner las catarinas en el centro del plato viéndose cada una de enfrente)"],
               ingredients:
            ["Queso mozzarella","Tomate cherry ","Albahaca ","Aceitunas negras sin hueso ","Aceite de oliva"]
        ),
        Recipe(id:2,title: "Ensalada de atún", image: "recipe2.png", steps:
            [
                "Lavar y desinfectar muy bien todas las verduras y frutas.",
                "Picar el jitomate, palmito, aceitunas y cilantro.",
                "En un plato vaciar media lata de atún y poner todos los ingredientes que se picaron.",
                "Agregar media cucharadita de aceite de oliva y revolver todos los ingredientes.",
                "Poner el atún en un topper. Agregar un paquete de galletas salmas para acompañar.",
                "Mandar en otro topper media manzana verde picada. Se puede agregar chile en polvo si el niño come picante.",
                "Poner todo en una lonchera con tenedor y agua simple.",
                "Montaje: disperso (poner todo en toppers para llevar",
                
                
],
            ingredients:
            ["Atún en lata de agua","Jitomate","Palmito en lata ","Palmito en lata ","Cilantro ","Aceite de oliva ","Salmas ","Manzana verde "] )
        , Recipe(id:3,title: "Sándwich de pan árabe con atún", image: "recipe3.png", steps:
            ["Lavar y desinfectar muy bien todas las verduras y frutas.",
                "Ya lavados y desinfectados cortar la lechuga en chiffonade y el jitomate en jardiniere.",
                "Drenar la lata de atún en agua.",
                "En la mitad del pan poner el atún, jitomate, espinaca y lechuga.",
                "Poner en un topper para llevar.",
                "En otro topper poner el melón picado.",
                "Poner todo en una lonchera con tenedor y agua simple.",
                "Mandar un termo con agua natural.",
                "Montaje:  estructurado (poner toda la comida en toppers separados).",
                
                
],
            ingredients:
            ["Atún en lata en agua","Pan árabe","Espinacas baby","Lechuga","Jitomate","Melón picado"]
        )
    ]
    //paso 1 agregar nuevos datos en formato JSON
    //let datosJSON = "[ {\"marca\": \"FORD\", \"agencias\": 21}, {\"marca\": \"BMW\", \"agencias\": 35} ]"
    //paso 2 declarar una variable para contener los nuevos datos
    
    //paso 3 incluir una funcion que convierta de JSON a Array
    func JSONParseArray(_ string: String) -> [AnyObject]{
        if let data = string.data(using: String.Encoding.utf8){
            
            do{
                
                if let array = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)  as? [AnyObject] {
                    return array
                }
            }catch{
                
                print("error")
                //handle errors here
                
            }
        }
        return [AnyObject]()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //paso 4 llenar el nuevo arreglo
        //nuevoArray=(JSONParseArray(datosJSON) as NSArray) as! [Any]
        //let datos = try? Data(contentsOf: url!)
        // nuevoArray = try! JSONSerialization.jsonObejct(with: datos!)
        //pas09
        //let url = URL(string: direccion)
        //let datos = try? Data(contentsOf: url!)
        //nuevoArray = try! JSONSerialization.jsonObject(with: datos!) as? [Any]
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //return datos.count
        
        //paso 5 remplazar el uso de datos por nuevoArray
        return (datos.count)
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let recipe = datos[indexPath.row] as Recipe
        let cell = tableView.dequeueReusableCell(withIdentifier: "EntryRecipe", for: indexPath)
        
        // Configure the cell...
        //cell.textLabel?.text = datos[indexPath.row]
        //paso 6 remplazar el uso de datos por nuevoArray
        //objeto marca es un diccionario que contiene marca y agencias
       
        let s:String = recipe.title! as String
        
        cell.textLabel?.text=s
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        let siguienteVista = segue.destination as! RecipeDetailViewController
        let indice = self.tableView.indexPathForSelectedRow?.row
      
        //paso 7 remplazar el uso de datos por nuevoArray
        //objeto marca es un diccionario que contiene marca y agencias
        let recipe = datos[indice!] // as recipe
        siguienteVista.receta = recipe
    }
}
