//
//  ILITableViewCell.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 3/22/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class ILITableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconimage: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDetail: UILabel!
    @IBOutlet weak var socialIcon: UIImageView!  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
