//
//  Ingredient.swift
//  FitLife
//
//  Created by Lawrie Cate on 26/03/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import Foundation

class Ingredient {
    var title:String?
    var quantity_mx:String?
    var quantity_g:Int?
    var cost_mx:Float?
    var kcal:Float?
    
    init(title:String,quantity_mx:String,quantity_g:Int,cost_mx:Float,kcal:Float) {
        self.title = title
        self.quantity_mx = quantity_mx
        self.quantity_g = quantity_g
        self.cost_mx = cost_mx
        self.kcal = kcal
        //super.init()
    }
    
    
}
