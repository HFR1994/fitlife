//
//  RecipeTabViewController.swift
//  FitLife
//
//  Created by Lawrie Cate on 01/04/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class RecipeTabViewController: UITabBarController {
    var receta:Recipe!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(receta != nil) {

           // if( self.selectedViewController?.restorationIdentifier == "procedimiento") {
                let selectedController = self.viewControllers?.first as! RecipeDetailViewController
                selectedController.receta = self.receta
            
            let ingController = self.viewControllers?[1] as! RecipeIngredientsViewController
            ingController.ingredients = self.receta.ingredients!
                           // }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "procedimiento") {
            let siguienteVista = segue.destination as! RecipeDetailViewController
            siguienteVista.receta = self.receta
        }
        else if(segue.identifier == "ingredientes") {
            let siguienteVista = segue.destination as! RecipeIngredientsViewController
            siguienteVista.ingredients = self.receta.ingredients!
        }
    }
 

}
