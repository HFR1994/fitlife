//
//  Recipe.swift
//  FitLife
//
//  Created by Lawrie Cate on 20/03/2017.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import Foundation

class Recipe {
    var id:Int?
    var title:String?
    var image:String?
    var steps:[String]?
    var ingredients:[String]?
    
    init(id:Int,title:String, image:String, steps:[String], ingredients:[String]) {
        self.id = id
        self.title = title
        self.image = image
        self.steps = steps
        self.ingredients = ingredients
        
        //super.init()
    }
    
 
}
