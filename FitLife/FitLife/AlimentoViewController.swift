//
//  AlimentoViewController.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 5/4/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class AlimentoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var items: [String] = []
    
    @IBOutlet weak var tablaInicial: UITableView!
    @IBOutlet weak var tablaProcedimiento: UITableView!
    @IBOutlet weak var tablaAlimento: UITableView!
    
    var const=["Tiempo Total","Porciones", "Calorias","Costo"]
    
    var meta = ["40 min","2 porciones","115 cal","40 pesos"]
        
    var alimento=[
        ["½ taza de harina",
         "1 pieza de huevo",
         "¼ de taza de leche",
         "½ cucharadita de royal",
         "1 cucharadita de azúcar",
         "1 cucharadita de mantequilla",
         "2 piezas de mandarina o naranja",
         "½ pieza de plátano",
         "2 moras azules",
         "½ vaso de leche semidescremada"],
        ["Mezclar la harina , el royal y el azúcar en un bowl",
         "Agregar huevo, la leche, y una cucharadita de mantequilla derretida y mezclar hasta que se haya formado una mezcla uniforme.",
         "Calentar el satén y poner tantita mantequilla",
         "Poner un poco de la mezcla en el sartén para hacer el hotcake"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tablaInicial.delegate=self
        self.tablaInicial.dataSource=self
        self.tablaAlimento.delegate=self
        self.tablaAlimento.dataSource=self
        self.tablaProcedimiento.dataSource=self
        self.tablaProcedimiento.delegate=self
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (tableView == self.tablaInicial) ? const.count : (tableView == self.tablaAlimento) ? alimento[0].count : alimento[1].count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(tableView == self.tablaInicial){
            let cell = tableView.dequeueReusableCell(withIdentifier: "tablaInicial", for: indexPath) as! tablaInicial
            
            cell.Procedimiento.text = const[indexPath.row]
            cell.Meta.text = meta[indexPath.row]
            
            return cell
        }else if (tableView == self.tablaAlimento){
            let cell = tableView.dequeueReusableCell(withIdentifier: "tablaAlimento", for: indexPath) as! tablaAlimento
            
            cell.Alimento.text = alimento[0][indexPath.row]
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "tablaProcedimiento", for: indexPath) as! tablaProcedimiento
            
            cell.List.text = "\(indexPath.row+1)"
            cell.Instruccion.text = alimento[1][indexPath.row]
            
            return cell
        }
    }

}
