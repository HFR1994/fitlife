//
//  DetailsViewController.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 3/23/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var imagen: UIImageView!
    @IBOutlet weak var tableview: UITableView!
    
    var items: [String:String] = [:]
    var array: [String:String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource=self
        tableview.delegate=self
        items=array
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count+1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch(indexPath.row){
            
        case 0:
            let cell=tableView.dequeueReusableCell(withIdentifier: "Basico", for: indexPath) as! ILTableViewCell
            cell.iconimage.image = UIImage(named: "doctor.png")
            cell.lbl.text = items["Nombre"]!
            return cell
        case 1:
            let cell=tableView.dequeueReusableCell(withIdentifier: "Detallado",for:indexPath) as! ILITableViewCell
            if(items["Facebook"] != nil){
                cell.lblTitle.text = items["Facebook"]
                cell.lblDetail.text = "Facebook"
                cell.iconimage.image = UIImage(named: "facebook.png")
            }else{
                cell.lblTitle.text = items["Instagram"]
                cell.lblDetail.text = "Instagram"
                cell.iconimage.image = UIImage(named: "instagram.png")
            }
            return cell
        case 2:
            let cell=tableView.dequeueReusableCell(withIdentifier: "Detallado",for:indexPath) as! ILITableViewCell
            cell.lblTitle.text = items["Email"]
            cell.lblDetail.text = "Email"
            cell.iconimage.image = UIImage(named: "mail.png")
            return cell
        case 3:
            let cell=tableView.dequeueReusableCell(withIdentifier: "Detallado",for:indexPath) as! ILITableViewCell
            cell.lblTitle.text = items["Numero"]
            cell.lblDetail.text = "Movil"
            cell.iconimage.image = UIImage(named: "phone.png")
            return cell
        default:
            let cell=tableView.dequeueReusableCell(withIdentifier: "Basico", for: indexPath) as! ILTableViewCell
            cell.iconimage.image = UIImage(named: "glasses.png")
            cell.lbl.text = "Ver Recetas"
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
