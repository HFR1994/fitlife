//
//  ILTableViewCell.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 3/22/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class ILTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var iconimage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
