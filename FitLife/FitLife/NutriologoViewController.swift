//
//  NutriologoViewController.swift
//  FitLife
//
//  Created by Héctor Carlos Flores Reynoso on 3/22/17.
//  Copyright © 2017 FitLifeTeam. All rights reserved.
//

import UIKit

class NutriologoViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {

    var items: [String] = []
    var contacto: [Int:[String:String]] = [:]
    var index: Int = 0
    
    @IBOutlet weak var tableview: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell=tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text=items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        index=indexPath[1]
        performSegue(withIdentifier: "ViewDetail", sender: self)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! DetailsViewController
        print(index)
        destination.array=contacto[index]! as [String : String]
        destination.imagen?.image = UIImage(named: "nutriologo.png")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.delegate=self
        tableview.dataSource=self
        items.append("Juan Fernandez")
        items.append("Jazmin Júarez")
        
        var temp: [String:String] = [
            "Nombre":"Juan Fernandez",
            "Facebook":"nutrijuan",
            "Email":"nutrijuan@gmail.com",
            "Numero":"5556748349"
        ]
        
        contacto[0]=temp
        
        temp = [
            "Nombre":"Jazmin Júarez",
            "Instagram":"Nutriflor",
            "Email":"comeflores@nutricion.com",
            "Numero":"5567453423"
        ]
        
        contacto[1]=temp
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }

}
